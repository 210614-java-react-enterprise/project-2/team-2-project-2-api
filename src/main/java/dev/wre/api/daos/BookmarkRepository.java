package dev.wre.api.daos;
import dev.wre.api.models.Bookmark;
import dev.wre.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookmarkRepository extends JpaRepository<Bookmark,Integer> {

}
