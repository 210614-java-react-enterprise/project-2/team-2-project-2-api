FROM java:8
EXPOSE 5000
COPY target/*.jar .
ENTRYPOINT ["java","-jar","/app.jar"]

# FROM openjdk:8-jdk-alpine
# ARG JAR_FILE=target/*.jar
# CMD java -jar <app>.jar
